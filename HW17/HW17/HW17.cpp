﻿#include <iostream>

using namespace std;

class Vector
{
public:
    Vector() : x(0), y(0), z(0) {}
    Vector(double x, double y, double z) : x( x), y(y), z(z) {}
    void Show()
    {
        cout << '\n' << x << ' ' << y << ' ' << z << endl;
    }
    double Mod()
    {
        double Len = sqrt(pow(x,2) + pow(y, 2) + pow(z, 2));
        return Len;
    }
private:
    double x;
    double y;
    double z;
};

class Vort
{
public:
    Vort() : A(0), B(0) {}
    Vort(float A, float B) : A(A), B(B) {}
    void Show()
    {
        cout << '\n' << A << ' ' << B << endl;
    }
private:
    float A;
    float B;
};

int main()
{
    Vector v(10, 10, 10);
    v.Show();
    cout << v.Mod();
    Vort c(24, 12);
    c.Show();
}